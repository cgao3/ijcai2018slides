\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\usepackage{graphicx}

\usepackage{amsmath, amsfonts}
\usepackage{breqn}
\usepackage{amsthm}
\usepackage{times}
\usepackage{xcolor}
\usepackage{soul}
\usepackage[utf8]{inputenc}
\usepackage[small]{caption}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{breqn}
\usepackage{amsthm}
\usepackage{csquotes}
\usepackage[english]{babel}
%\usepackage[linkcolor=black]{hyperref}
\usepackage{url}

\graphicspath{{imgs/}}

\usepackage{xspace}

\newtheorem{proposition}{Proposition}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}


\title{Three-Head Neural Network Architecture for Monte Carlo Tree Search}
\subtitle{ IJCAI 2018}
% \date{\today}
\date{}
\author{Chao Gao, Martin M\"{u}ller, Ryan Hayward}
\institute{Department of Computing Science, University of Alberta}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

%\begin{frame}{Table of contents}
%  \setbeamertemplate{section in toc}[sections numbered]
%  \tableofcontents[hideallsubsections]
%\end{frame}

%\section{Introduction}

\begin{frame}[fragile]{PV-MCTS: two-player game of Go}

  Two-head neural networks can be used in Monte Carlo Tree Search (MCTS), where the policy output is used for prior
  action probability and the state-value estimate is
  used for leaf node evaluation. 
  \begin{figure}
  \includegraphics[scale=0.3]{pv-mcts-2h.png}
  \caption{PV-MCTS used in AlphaGo Zero; Image from Silver et al. 2017.}
  \end{figure}
\end{frame}
\begin{frame}[fragile]{Delayed Node Expansion}
But, consider the case that you have a delayed node expansion. 

\begin{figure}[tp]
\centering
\includegraphics[scale=1.0]{tree1.pdf}
\caption{Two-head architecture in PV-MCTS: The leaf node must be expanded (a) with threshold 0, otherwise no neural net value estimation can be backed up (b). $\hat{f}_\theta$ represents a two-head neural net that each evaluation of state $s$ yields a vector of move probabilities $\mathbf{p}$ and state-value $v$. $\mathit{N(s)}$ is the visit count of $s$. } \label{fig:tree1}
\end{figure}

\end{frame}

%\section{Titleformats}

\begin{frame}{Three-Head Neural Network}
A three-head neural network provides policy, state- and action-value estimates.  

\begin{figure}[htp]
\centering
\includegraphics[scale=1.0]{tree2.pdf}
\caption{PV-MCTS with three-head neural net $f_\theta$: The leaf node be expanded with any threshold. The action-value estimate can be backed up even if the leaf has not been expanded, because it was stored there upon node creation. } \label{fig:tree2}
\end{figure}
\end{frame}

\begin{frame}{How advantageous is 3HNN over 2HNN?}
Denote PV-MCTS-2H as PV-MCTS using two-head net,
PV-MCTS-3H as PV-MCTS using three-head net. 


It is apparent that, to have an efficient usage of neural net evaluations, PV-MCTS-2H can either 
\begin{itemize}
\item 1) restrict the expansion threshold to be 0, or 
\item 2) still use playout result when the leaf node is below an expansion threshold $\zeta \geq 1$. 
\end{itemize}

Now we analyse the differences in number of neural leaf estimates between PV-MCTS-2H and PV-MCTS-3H.  
\end{frame}

\begin{frame}{How advantageous is 3HNN over 2HNN?}
For case 1), suppose PV-MCTS-2H and PV-MCTS-3H are allocated with the same amount of computation time $\tilde{T}$ on the same hardware.  

Let $t$  and $t'$ be respectively the one simulation time cost for PV-MCTS-2H and PV-MCTS-3H. 

Since $t' \leq t$, the number of  neural leaf estimates received by PV-MCTS-3H is $\frac{\tilde{T}}{t'} - \frac{\tilde{T}}{t}$ more than that of PV-MCTS-2H.  

\end{frame}

\begin{frame}{How advantageous is 3HNN over 2HNN?}
For case 2), PV-MCTS-3H and PV-MCTS-2H consume the same computation time for the same number of simulations, we have the following observation:

\begin{proposition} \label{propsition:1}
Suppose the total number of simulations for MCTS is $T$, expansion threshold is $\zeta \geq 1$, after the search terminates, PV-MCTS-3H receives at least $T - \frac{T}{\zeta}$ more neural leaf estimates than PV-MCTS-2H. 
\end{proposition} 
\end{frame}

\begin{frame}{How to train 3HNN?}

For a given set of games $\mathcal{D}$, review the loss function described to train 2H:

\begin{dmath} \scriptsize \label{lossold}
\hat{L}(\hat{f}_\theta ; \mathcal{D})= \sum_{(s,a,z_s) \in \mathcal{D}} \Bigg( w (z_s - v(s))^2 - \log p(a|s) + c ||\theta||^2  \Bigg) 
\end{dmath} 
$0<w \leq 1$ is a weighting parameter. 

$c$ is a constant for the level of $L_2$ regularization. 

$(s,a)$ is a state-action pair from dataset $\mathcal{D}$,

and $z_s$ is the game result with respect to player to play at $s$.  

\end{frame}

\begin{frame}{How to train 3HNN?}
 
We need to answer the following question: 

\vspace*{0.2cm}
{{\Large  Could we train 3HNN on the same dataset $\mathcal{D}$? }}

\vspace*{1cm}
We achieve this by exploiting reward properties in two-player games. 
\end{frame}

\begin{frame}{How to train 3HNN? Data Augmentation}

For a game with two outcomes, either first player win or second player win. Then, for any given game state $s$, the following relations hold:
\begin{itemize}
\item The OR constraint: if $s$ is winning, then at least one action leads to a losing state for the opponent.
\item The AND constraint: if $s$ is losing, then \textit{all} actions lead to 
a winning state for the opponent.
\end{itemize}
\end{frame}

\begin{frame}{How to train 3HNN? Data Augmentation}
According to the AND constraint, for each game $g \in \mathcal{D}$, $g$ implies a tree rather than a single trajectory.

\begin{figure}[tp] \scriptsize
\centering
\includegraphics[scale=1.0]{tree3.pdf}
\caption{A game implies extra data via the AND constraint.} \label{fig:tree3}
\end{figure}
\end{frame}

\begin{frame}{How to train 3HNN? Data Augmentation}
Reflecting in the loss function, we introduce the following loss term:
\begin{dmath} \scriptsize
L_Q(f_\theta ; \mathcal{D})= \sum_{(s,a,z_s) \in \mathcal{D}} \frac{\max(-z_s,0)}{|\mathcal{A}(s)|}\sum_{a' \in \mathcal{A}(s)}(z_s + q(s,a'))^2
\end{dmath} 
where $\mathcal{A}(s)$ is the action set at $s$.
\end{frame}

\begin{frame}{How to train 3HNN? Optimal penalty}
A two-player alternate-turn zero-sum game can be formulated as an Alternating Markov Game (AMG).
For an AMG, the optimal Bellman equation can be expressed as:
 
\begin{equation}
v^*(s) =- \min_{a} q^*(s,a), s \in \mathcal{S},
\end{equation} 
$v^*$ and $q^*$ are the optimal state-value and action-value functions. 

It expresses the consistency of state and action values under an optimal policy.

\end{frame}


\begin{frame}{How to train 3HNN? Optimal penalty}
 To force the state and action values to satisfy the optimal consistency, we add the optimal Bellman error as a quadratic penalty.  

\begin{dmath} \scriptsize
L_P(f_\theta ; \mathcal{D})= \sum_{(s,a,z_s) \in \mathcal{D}} (\min_{a'}q(s,a') + v(s))^2 
\end{dmath} 
\end{frame}


\begin{frame}{How to train 3HNN? A new target}
Combining the usual state-, action-value and policy losses, we propose the following loss function: 

\begin{dmath} \scriptsize \label{lossnew}
L(f_\theta ; \mathcal{D})= \sum_{(s,a,z_s) \in \mathcal{D}} \Bigg( w \bigg( \frac{1}{2}(z_s - v(s))^2 + \frac{1}{2}(z_s + q(s,a))^2  \bigg) - \log p(a|s) + c ||\theta||^2 \Bigg) + w L_Q +wL_P
\end{dmath}
\end{frame}

\begin{frame}{Experiments on the Game of Hex}
\textbf{Dataset:} Publicly available training dataset of MoHex-CNN, generated from MoHex 2.0 self-play.

About $10^6$ distinct state-action-value examples.

\textbf{Neural Net Design:}
\begin{figure}[tp]
\centering
\includegraphics[width=0.45\textwidth,height=0.35\textwidth]{architecture.pdf}
\caption{Each residual block has 2 convolution layers, each with 32 $3\times 3$ filters.} \label{fig:architecture}
\end{figure}
\end{frame}

\begin{frame}{Experiments: Training result}
We compare to a 2HNN with identical residual architecture as our 3HNN except that it has only policy and state-value heads.

\begin{figure}[tp]
\centering
\includegraphics[width=0.65\textwidth]{v.pdf}
\caption{Mean Square Errors of two and three heads residual nets.} \label{fig:nnValue}
\end{figure}

\end{frame}

\begin{frame}{Experiments: Training result}

\begin{figure}[tp] \small
\centering
\includegraphics[scale=0.55]{p.pdf}
\caption{MSEs (left) and top one move prediction accuracies (right) of two and three heads residual nets.}  \label{fig:nnPolicy}
\end{figure}
\end{frame}

\begin{frame}{Combined with the same PV-MCTS}
We compared MoHex-2HNN and MoHex-3HNN by playing against MoHex-CNN. 
\begin{figure}[htp]
\centering
\includegraphics[scale=0.75]{mcts2headvsmcts3head.pdf}
\caption{Winrates against MoHex-CNN of PV-MCTS-2H and PV-MCTS-3H. All programs use the same $1000$ simulations per move. PV-MCTS-2H uses playout results when there is no node expansion. }
\end{figure}
\end{frame}


\begin{frame}{Combined with the same PV-MCTS}

We then compare PV-MCTS-2H to PV-MCTS-3H using expansion threshold of $0$. We give both programs the same search time $10s$ per move. 


\begin{table}[htp!] \scriptsize
\centering
\begin{tabular}{c  p{10mm}  p{10mm}  p{10mm}  p{10mm}  }
\toprule
Player  & Player as black & Player as white & Overall  winrate  \\ \midrule 
MoHex-3HNN  & 76.5\% & 70.6\%  & 73.5\%  \\ 
MoHex-2HNN threshold 0  & 65.9\% & 57.6\%  & 61.8\% \\
MoHex-2HNN default threshold & 69.4\% & 56.5\%  & 62.9\% \\
\bottomrule  
\end{tabular}
\caption{Winrates against Mohex-CNN with the same time per move. For best performance, MoHex-3HNN and MoHex-2HNN respectively use the neural net models at epochs 70 and 60.} \label{tab:sametime1}
\end{table}
\textbf{Reason:} MoHex-3HNN is 6 times faster than MoHex-2HNN to conduct one simulation. 
\end{frame}

\begin{frame}{Strengthen the Leaf Estimate}
When expanding a node $s$, PV-MCTS-3H receives an additional vector of action-values. 

Rather than solely using the state-value head, could we improve the leaf estimate by also considering the action-values? 
\end{frame}


\begin{frame}{Strengthen the Leaf Estimate -- First Attempt}
Combine the minimum action-values and state-value at $s$ when they are both available, by the following formula: 
\begin{equation} \label{eq:adversarial}
v^{\circ}(s) = \frac{1}{2} v(s) + \frac{1}{2}\big( -\min_{a\in \mathcal{A}(s)} q(s,a)\big) 
\end{equation} 

\begin{table}[htp] \scriptsize
\centering
\begin{tabular}{c  p{10mm}  p{9mm}  p{9mm}  p{10mm}  }
\toprule
Opponent  & Opponent as black & Opponent as white & Overall  winrate  \\ \midrule 
MoHex-3HNN  & 48.2\% & 68.2\%  & 58.2\%  \\ 
\bottomrule  
\end{tabular}
\caption{Winrates of MoHex-3HNN$^{\circ}$ against MoHex-3HNN with the same time $10s$ per move. Both use the neural net models at epoch 70. }
\label{tab:sametime2}
\end{table}

\end{frame}


\begin{frame}{Strengthen the Leaf Estimate -- Second Attempt}

The second approach is to utilize the OR constraint, i.e., one losing child is enough to prove that the parent state $s$ is winning. 
If there are $k>1$ action-values close to $-1$, the confidence that $v^*(s)=+1$ would be high.  
We may say an action-value prediction from $\mathbf{q}$ is ``correct'' when $q(s,a)<\delta \wedge q^*(s,a)=-1$. 
Suppose there are $k$ action-values below $\delta$, and each of them is correct with probability $\bar{p}$, then the chance that $v^*(s)=+1$ is $1-(1-\bar{p})^k$. 

Setting $\delta=-0.5$, our second attempt is to let MoHex-3HNN replace $v(s)$ with $\frac{1}{2}(1+v(s))$ when at least $k$ action-values from $\mathbf{q}$ are ``correct``.

\begin{table}[htp] \scriptsize
\centering
\begin{tabular}{c  p{10mm}  p{9mm}  p{9mm}  p{10mm} p{10mm} }
\toprule
Opponent  & $k=3$ & $k=4$ & $k=5$  & $k=6$ \\ \midrule 
MoHex-3HNN  & 47.6\% & 52.4\%  & 54.7\%  & 54.7\% \\ 
\bottomrule  
\end{tabular}
\caption{Different winrates of MoHex-3HNN$^{*}$ against MoHex-3HNN by varying $k$. Both use the same neural net model at epoch 70 and are allocated with the same $10s$ per move. }
\label{tab:sametime3}
\end{table}
\end{frame}


\begin{frame}{Conclusions}

\begin{itemize}
\item 3HNN improved search efficiency by allowing PV-MCTS to have delayed node expansion
\item In two-player games, 3HNN can be trained on the same dataset as for 2HNN
\item Leaf estimate may be strengthened by combining state and action-value estimates
\end{itemize}
\end{frame}
\end{document}
